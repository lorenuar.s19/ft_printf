/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extra.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/21 18:53:35 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/03 23:42:06 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

ssize_t	put_char(char c)
{
	return (write(1, &c, 1));
}

size_t	str_len(char *s)
{
	size_t	i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

size_t	sub_put_nbr_unsigned(size_t num, size_t base, char *b_chars)
{
	ssize_t	ret;

	ret = 0;
	if (num >= base)
	{
		ret += sub_put_nbr(num / base, base, b_chars);
		ret += sub_put_nbr(num % base, base, b_chars);
	}
	if (num < base)
	{
		put_char(b_chars[num]);
		ret += 1;
	}
	return (ret);
}
