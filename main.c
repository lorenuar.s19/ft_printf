/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 10:11:39 by lorenuar          #+#    #+#             */
/*   Updated: 2020/09/23 01:02:38 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <fcntl.h>

/*
** printf("|\nSTDIO < R %08d >\n", printf("STDIO : |" TEST ));
** ft_printf("|\nFT_42 < R %08d >\n", ft_printf("FT_42 : |" TEST ));
*/

# define TEST "-->|%-16.p|<--\n", p

int		main(void)
{
	char *p = NULL;
	printf("|\nSTDIO < R %08d >\n", printf("STDIO : |" TEST ));
	ft_printf("|\nFT_42 < R %08d >\n", ft_printf("FT_42 : |" TEST ));
	return (0);
}
