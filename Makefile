# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/10 13:37:24 by lorenuar          #+#    #+#              #
#    Updated: 2020/09/22 23:13:28 by lorenuar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


# ================================ VARIABLES ================================= #
SHELL = bash

ifndef $(DEBUG)
	DEBUG=0
endif

NAME	= libftprintf.a

cc 		= gcc
# ifeq ($(DEBUG),0)
# 	CFLAGS 	= -Wall -Wextra -Werror
# endif

ifeq ($(DEBUG),1)
	CFLAGS	+= -g3 -D DEBUG=1 -fsanitize=address
else
	CFLAGS 	= -Wall -Wextra -Werror
	CFLAGS	+= -D DEBUG=0
endif
CFLAGS	+= -I $(INCDIR)

SRCDIR	= src/
INCDIR	= incs/
OBJDIR	= objs/



###▼▼▼<src-updater-do-not-edit-or-remove>▼▼▼
# **************************************************************************** #
# **   Generated with https://github.com/lorenuars19/makefile-src-updater   ** #
# **************************************************************************** #
SRCS = \
	src/utils/num.c \
	src/utils/str.c \
	src/ft_printf.c \
	src/numbers.c \
	src/strings.c \
	src/unsigned.c \
	main.c \

###▲▲▲<src-updater-do-not-edit-or-remove>▲▲▲


SRC		:= $(notdir $(SRCS))#                               Files only
OBJ		:= $(SRC:.c=.o)#                                    Files only
OBJS	:= $(addprefix $(OBJDIR), $(OBJ))#                  Full path
CSRCS	:= $(addprefix ../, $(SRCS))#                       Compiler

GR	=\033[32;1m#                                            Green
RE	=\033[31;1m#                                            Red
YE	=\033[33;1m#                                            Yellow
CY	=\033[36;1m#                                            Cyan
RC	=\033[0m#                                               Reset Colors

# ================================== RULES =================================== #

all : $(NAME)

#	linking
$(NAME)	: $(OBJS)
	@printf "$(YE)&&& Linking $(OBJ) to $(NAME)$(RC)\n"
	@ar -rcs $(NAME) $(OBJS)

#	compiling
$(OBJS) : $(SRCS)
	@printf "$(GR)+++ Compiling $(SRC) to $(OBJ)$(RC)\n"
	@mkdir -p $(OBJDIR)
	cd $(OBJDIR) && $(CC) -I ../$(INCDIR) $(CFLAGS) -c $(CSRCS)

# 	updating
update:
ifeq ($(shell uname),Darwin)
	@printf "$(CY)Updating the repo$(RC)\n"
	@sleep 1
	git pull --rebase --force --autostash
endif

#	running
ifeq ($(shell uname),Darwin)
run : update re $(NAME)
else
run : re $(NAME)
endif
	@printf "$(YE)&&& Linking with $(NAME) into test_printf$(RC)\n"
	$(CC) $(CFLAGS) main.c $(NAME) -o test_printf

ifeq ($(DEBUG),1)
	-@rm -f log
	-@grep  "^# define TEST" main.c >> log

ifeq ($(shell uname),Linux)
	-timeout 5 ./test_printf >> log
else
	-./test_printf >> log
endif

	-@printf "\nCheck Return < R %08d >\n" $$(echo $$(grep -e "^FT_42 : " log | wc -m | tr -d " log" ) " - 2" | bc ) | cat -vte >> log
	-@rm -f test_printf
#	sleep 5
	-@read -n 1 -p "Press any key to continue ..."
	-@less -ecNK log
	-@rm -f log
endif
#	@make fclean

#	cleaning
clean :
	@echo "$(RE)--- Removing $(OBJ)$(RC)"
	@rm -rfd $(OBJS) $(OBJDIR)

fclean : clean
	@echo "$(RE)--- Removing $(NAME)$(RC)"
	@rm -f $(NAME) test_printf

re : fclean all

debug :
	@echo "SRCS $(SRCS)"
	@echo "SRC $(SRC)"
	@echo "OBJS $(OBJS)"
	@echo "OBJ $(OBJ)"
	@echo "CSRCS $(CSRCS)"
	@echo "CFLAGS $(CFLAGS)"

.PHONY	= all run clean fclean re debug
